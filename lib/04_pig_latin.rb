def translate(sentence)
  words = sentence.split
  words.each do |word|
    (0...word.length).each do |index|
      if consonants(word[index])
        word << word[index]
      else
        word[0..-1] = word[index..-1] << "ay"
        break
      end
    end
  end
  words.join(" ")
end

def consonants(letter)
  vowels = ["a","e","i","o"]
  consonants = ("a".."z").to_a - vowels

  if consonants.include?(letter)
    return true
  else
    false
  end
end
