def echo(word)
  word
end

def shout(word)
  word.split.map do |el|
    el.upcase
  end.join(" ")
end

def repeat(word,t=2)
  array = []
  t.times do |x|
    array << word
  end
  array.join(" ")
end

def start_of_word(word,index)
  word[0..index-1]
end

def first_word(sentence)
  array = []
  array = sentence.split
  array[0]
end

def titleize(sentence)
  answer = []
  exceptions = ["the","over","and"]
  words = sentence.split(" ")

  words.each_with_index do |word,index|
    if exceptions.include?(word) && index > 0
      answer << "#{word}"
    else
      answer << "#{word[0].upcase}#{word[1..-1].downcase}"
    end
  end
  answer.join(" ")
end
