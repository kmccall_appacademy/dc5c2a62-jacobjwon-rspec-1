def add(num,num2)
  num + num2
end

def subtract(num,num2)
  num - num2
end

def sum(array)
  return 0 if array == []
  array.reduce(:+)
end

def multiply(array)
  array.reduce(:*)
end

def power(num,num2)
  num ** num2
end

def factorial(num)
  (1..num).reduce(:*)
end
